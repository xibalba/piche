/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Mixin WidgetIdeable
 * This Mixin allow to widget classes set a string as an id.
 */
qx.Mixin.define('piche.mixins.WidgetIdeable', {
    properties: {
        widgetId: {check: 'String', init: ''}
    }
});
