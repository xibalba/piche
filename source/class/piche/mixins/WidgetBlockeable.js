/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2016 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This Mixin provide to Widget classes «block» and «unblock» methods.
 * Those methods block the widget and show/hide a loading image.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @asset(piche/loading66.gif)
 */
qx.Mixin.define('piche.mixins.WidgetBlockeable', {
	include : [qx.ui.core.MBlocker],

	construct: function () {
		this.block = this.__block;
		this._createBlocker = this.__createBlocker;
	},

	members: {
		__createBlocker: function() {
			var blocker = new qx.ui.core.Blocker(this);

			blocker.setColor("#D6D6D6");
			blocker.setOpacity(0.6);

			blocker.getBlockerElement().setStyle('text-align','center');

			//The helper allow to set loading image at center vertically.
			var helper = new qx.html.Element('span');
			helper.setStyles({
				'text-align': 'center',
				'display': 'inline-block',
				'height': '100%',
				'vertical-align': 'middle'
			});

			var loadingImage = new qx.html.Element('img');
			loadingImage.setAttribute('src', qx.util.ResourceManager.getInstance().toUri('piche/loading66.gif'));
			loadingImage.setStyle('vertical-align', 'middle');

			blocker.getBlockerElement().add(helper);
			blocker.getBlockerElement().add(loadingImage);

			return blocker;
		},

		__block: function() {
			if(this instanceof qx.ui.window.Window) this.getBlocker().getBlockerElement().setStyle('z-index', this.getZIndex() + 100);
			this.getBlocker().block();
		}
	}
});
