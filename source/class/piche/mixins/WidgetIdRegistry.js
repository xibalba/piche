/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Mixin WidgetIdRegistry
 * This Mixin allow to classes have a string widget registry.
 * When a Widget is created, if include WidgetIdeable Mixin, then
 * is possible get after that Widget via id.
 *
 * ```javascript
 *     //Define a Widget than include WidgetIdeable Mixin.
 *     qx.Class.define('CustomWidget', {
 *         extend: qx.ui.core.Widget,
 *         include: [piche.mixins.WidgetIdeable],
 *
 *         construct: function(widgetId) {
 *             this.base(arguments);
 *             if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.setWidgetId(widgetId);
 *         }
 *     });
 *
 *     //Define a Contirer than include WidgetIdRegistry Mixin.
 *     qx.Class.define('CustomContainer', {
 *         extend: qx.ui.container.Composite,
 *         include: [piche.mixins.WidgetIdRegistry]
 *
 *         //Maybe you want set an auto registration of widgets
 *         construct: function() {
 *             this.base(arguments);
 *
 *             this.addListener('addChildWidget', this._onWidgetAdded, this);
 *             this.addListener('removeChildWidget', this._onWidgetRemoved, this);
 *         }
 *     });
 *
 *     //Create a Widget instance and add it to Contairer
 *     var widget = new CustomWidget('foo');
 *     var container = new CustomContainer();
 *     container.add(widget);
 *
 *     //So, now you can get the widget via id
 *     var widgetGettedById = container.getWidgetById('foo');
 * ```
 */
qx.Mixin.define('piche.mixins.WidgetIdRegistry', {
    members: {

        _getWidgetMap: function() {
            var idWidgetMap = this.getUserData('idWidgetMap');
            if(idWidgetMap) return idWidgetMap;
            else {
                idWidgetMap = [];
                this.setUserData('idWidgetMap', idWidgetMap);
                return idWidgetMap;
            }
        },

        getWidgetById: function(id) {
            return this._getWidgetMap()[id];
        },

        register: function(widget, id) {
            var idWidgetMap = this._getWidgetMap();

            if(idWidgetMap[id]) throw new Error("A widget with the id '"+id+"' already exists on" + this.classname + ".");
            idWidgetMap[id] = widget;

            if (qx.core.Environment.get("qx.debug")) {
                console.info('Register on '+ this.classname + ' the widget id: ' + id);
            }
        },

        unregister : function(widget, id) {
            var idWidgetMap = this._getWidgetMap();

            if(idWidgetMap[id] !== widget) throw new Error("The widget is not registered with the id '"+id+"'.");
            delete(idWidgetMap[id]);

            if (qx.core.Environment.get("qx.debug")) {
                console.info('Unregister on '+ this.classname + ' the widget id: ' + id);
            }
        },

        _onWidgetAdded: function(dataEvent) {
            var widget = dataEvent.getData();
            var widgetId = widget.getWidgetId();
            if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.register(widget, widgetId);
        },

        _onWidgetRemoved: function(dataEvent) {
            var widget = dataEvent.getData();
            var widgetId = widget.getWidgetId();
            if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.unregister(widget, widgetId);
        }
    }
});

