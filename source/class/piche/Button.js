/* ************************************************************************

   Copyright: 2023 Yeshua Rodas

   License: MIT license

   Authors: Yeshua Rodas (yybalam) yrodas@upnfm.edu.hn

************************************************************************ */

/**
 * This is an example of a contrib library, providing a very special button 
 * @asset(piche/*)
 */
qx.Class.define("piche.Button",
{
  extend : qx.ui.form.Button,
});