/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * The Atom Field is a single-line non user editable form field
 * over the basic Atom component.
 */
qx.Class.define('piche.ui.form.AtomField', {
	extend: qx.ui.basic.Atom,

	implement: [
		qx.ui.form.IForm,
		qx.ui.form.IField
	],

	include: [
		qx.ui.core.MRemoteChildrenHandling,
		qx.ui.form.MForm
	],

	events: {
		/** Whenever the value is changed this event is fired
		 *
		 *  Event data: The new text value of the field.
		 */
		"changeValue": "qx.event.type.Data",
	},

	properties: {
		// overridden
		appearance: {
			refine: true,
			init: "atomfield"
		},
	},

	members: {
		/**
		 * @inheritdoc
		 */
		getValue() {
			return this.getLabel();
		},

		/**
		 * @inheritdoc
		 */
		setValue(value) {
			if(value !== null) this.setLabel(value + "");
			else this.setLabel(null);

			this.fireDataEvent("changeValue", value);
		},

		/**
		 * @inheritdoc
		 */
		resetValue() {
			this.setValue(null);
			this.setIcon(null);
		}
	}
});