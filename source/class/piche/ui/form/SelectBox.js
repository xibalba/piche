/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved SelectBox widget.
 * This class add support to set the list from a map to the
 * qooxdoo SelectBox widget.
 */
qx.Class.define('piche.ui.form.SelectBox', {
	extend: qx.ui.form.SelectBox,

	members: {
		/**
		 * Set a new list elements te SelectBox list.
		 *
		 * @param list {Array}
		 * @param keyKey {String}
		 * @param valueKey {String}
		 */
		setList: function(list, keyKey, valueKey) {
			this.removeAll();
			for (let i in list) this.addListElement(list[i], keyKey, valueKey);
		},

		/**
		 * Add a new element to current list.
		 *
		 * @param el {Object} Data map.
		 * @param keyKey {String}
		 * @param valueKey {String}
		 *
		 * @return addedItem
		 */
		addListElement: function(el, keyKey, valueKey) {
			keyKey = keyKey || 'key';
			valueKey = valueKey || 'value';

			const item = new qx.ui.form.ListItem(el[valueKey], null, el[keyKey]);
			this.add(item);
			return item;
		},

		/**
		 * If exist a ListItem seceted, thon return the getModel() value.
		 */
		getValue: function() {
			const selection = this.getSelection()[0];
			if(selection) return selection.getModel();
			return null;
		}
	}
});
