/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved Form widget.
 * The main feature of this class is provide «getData()» and «setData()» methods.
 * Use this way for an easy data manipulation.
 */
qx.Class.define('piche.ui.form.Form', {
	extend: qx.ui.form.Form,

	members: {
		/**
		 * Return an array with all items names.
		 * @returns {Array}
		 */
		getItemsNames: function() {
			let names = [];
			const groups = this.getGroups();
			// go threw all groups
			for (let  i = 0; i < groups.length; i++) {
				const group = groups[i];
				for (let  j = 0; j < group.names.length; j++) names.push(group.names[j]);
			}

			return names;
		},

		/**
		 * Set focus to the first item of the form.
		 */
		focusFirst: function() {
			this.getGroups()[0].items[0].focus();
		}
	}
});
