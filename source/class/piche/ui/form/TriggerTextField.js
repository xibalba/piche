/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 - 2016 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Class.define('piche.ui.form.TriggerTextField', {
    extend: qx.ui.core.Widget,

    implement: [
        qx.ui.form.IStringForm,
        qx.ui.form.IForm
    ],
    include: [ qx.ui.form.MForm ],

    construct: function(value, icon) {
        this.base(arguments);

        if(icon !== undefined) this.setIcon(icon);

        var layout = new qx.ui.layout.HBox();
        layout.setAlignY("middle");
        this._setLayout(layout);

        var field = this._createChildControl('textfield');
        this._createChildControl('button');

        // forward the focusin and focusout events to the textfield. The textfield
        // is not focusable so the events need to be forwarded manually.
        this.addListener("focusin", function(e) {
            field.fireNonBubblingEvent("focusin", qx.event.type.Focus);
            field.setTextSelection(0,0);
        }, this);

        this.addListener("focusout", function(e) {
            field.fireNonBubblingEvent("focusout", qx.event.type.Focus);
        }, this);

        if(value !== undefined) this.setValue(value);
    },

    properties: {
        // overridden
        appearance: {
            refine: true,
            init: "triggerfield"
        },
        // overridden
        focusable: {
            refine: true,
            init: true
        },

        value: {
            check: 'String',
            nullable: true,
            apply: '_applyValue',
            event: 'changeValue'
        },

        editable: {
            check: 'Boolean',
            init: true,
            apply: '_applyEditable'
        },

        icon: {
            check: 'String',
            nullable: true
        }
    },

    members: {
        // overridden
        _createChildControlImpl : function(id, hash) {
            var control;

            switch (id) {
                case "textfield":
                    control = new qx.ui.form.TextField();
                    control.setFocusable(false);
                    control.addState("inner");
                    this._add(control, {flex: 1});
                    break;
                case "button":
                    var icon = this.getIcon();
                    var label = (icon) ? null : '...';
                    control = new qx.ui.form.Button(label, icon);
                    control.setFocusable(false);
                    control.setKeepActive(true);
                    control.addState("inner");
                    this._add(control);
                    break;
            }

            return control || this.base(arguments, id);
        },

        /**
         * Apply routine for the value property.
         *
         * @param value {Number} The new value of the spinner
         * @param old {Number} The former value of the spinner
         */
        _applyValue: function(value, old) {
            var field = this.getChildControl('textfield');

            if(value !== null) field.setValue(value);
            else field.setValue('');
        },

        /**
         * Apply routine for the editable property.<br/>
         * It sets the textfield of the field to not read only.
         *
         * @param value {Boolean} The new value of the editable property
         * @param old {Boolean} The former value of the editable property
         */
        _applyEditable: function(value, old) {
            var field = this.getChildControl('textfield');
            if(field) field.setReadOnly(!value);
        },

        // overridden
        tabFocus: function() {
            var field = this.getChildControl('textfield');
            field.getFocusElement().focus();
            field.selectAllText();
        }
    }
});
