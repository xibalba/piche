/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license MIT:

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class extends the Qooxdoo List class adding support for store a Map of data
 * and an API to find items.
 */
qx.Class.define('piche.ui.form.List', {
	extend: qx.ui.form.List,
	include: [ piche.mixins.BusyBlocker ],

	construct: function(horizontal) {
		super(horizontal);
		this._listData = {};
	},

	members: {
		_listData: null,

		/**
		 * Internal method for extract the label String.
		 *
		 * @param item {qx.ui.form.ListItem}
		 * @private
		 * @return {String|null}
		 */
		__getItemLabel: function(item) {
			if (item.isRich()) {
				let control = item.getChildControl("label", true);
				if (control) {
					let labelNode = control.getContentElement().getDomElement();
					if (labelNode) return qx.bom.element.Attribute.get(labelNode, "text");
					else return null;
				}
			}
			else return item.getLabel();
		},

		/**
		 * Add ListItem instances from array.
		 * Create and set elements from an list and a templeate.
		 * The templeate must be formed by strings and tokens than will be used
		 * as keys on each element of passed list.
		 * Tho tokens must be between braces.
		 *
		 *   `tpl {{someKey}} and {{anotherKey}}`
		 *
		 * @param list {Array}
		 * @param template {String}
		 * @param keyKey {String} key name of list element
		 */
		addListItems: function(list, template, keyKey) {
			var splits = template.match(/\{\{\w+}}/g);
			var indexes = [];

			// First process the template
			for(var i in splits) {
				var el = splits[i];
				var ip = parseInt(i) + 1;
				el = el.replace('{{', '').replace('}}', '');
				indexes.push(el);
				template = template.replace('{{' + el + '}}', '%' + ip);
			}

			// Now process the list
			for (var it in list) {
				var item = list[it];
				var values = [];

				for(var a in indexes) values.push(item[indexes[a]]);

				this._listData[item[keyKey]] = item;
				var listItem = new qx.ui.form.ListItem(qx.lang.String.format(template, values), null, item[keyKey]);
				listItem.setRich(true);

				this.add(listItem);
			}
		},

		clean: function() {
			this.removeAll();
			this._listData = {};
		},

		/**
		 * Remove actual list items and add a new set.
		 * @see addListItems
		 *
		 * @param list {Array}
		 * @param template {String}
		 * @param keyKey {String} key name of list element
		 */
		setListItems: function(list, template, keyKey) {
			this.clean();
			this.addListItems(list, template, keyKey);
		},

		/**
		 * Find the first item which label contains the string search.
		 *
		 * @param search {String}
		 * @param caseSensitive {Boolean}
		 * @return {null|qx.ui.core.LayoutItem}
		 */
		findFirst: function(search, caseSensitive) {
			// lowercase search
			if (!caseSensitive) search = search.toLowerCase();

			// get all items of the list
			let items = this.getChildren();

			// go through all items
			for(let i in items) {
				let label = this.__getItemLabel(items[i]);

				if(label) {
					if(!caseSensitive) label = label.toLowerCase();
					if(label.includes(search)) return items[i];
				}
			}

			return null;
		},

		/**
		 * Return a set of items where their labels contains the search string.
		 *
		 * @param search {String}
		 * @param caseSensitive {Boolean}
		 * @return {qx.ui.form.ListItem[]}
		 */
		findItems: function(search, caseSensitive) {
			// lowercase search
			if (!caseSensitive) search = search.toLowerCase();

			// get all items of the list
			let items = this.getChildren();
			let result = [];

			// go through all items
			for(let i in items) {
				let label = this.__getItemLabel(items[i]);

				if(label) {
					if(!caseSensitive) label = label.toLowerCase();
					if(label.includes(search)) result.push(items[i]);
				}
			}

			return result;
		},

		getItemData: function(idx) {
			return this._listData[idx];
		}
	}
});
