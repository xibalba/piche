/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * The Atom Number Field is a single-line non user editable form field that support only number values
 * over the basic Atom component.
 */
qx.Class.define('piche.ui.form.AtomNumberField', {
	extend: qx.ui.basic.Atom,

	implement: [
		qx.ui.form.IForm,
		qx.ui.form.INumberForm
	],

	include: [
		qx.ui.core.MRemoteChildrenHandling,
		qx.ui.form.MForm
	],

	events: {
		/** Whenever the value is changed this event is fired
		 *
		 *  Event data: The new text value of the field.
		 */
		"changeValue": "qx.event.type.Data",
	},

	properties: {
		// overridden
		appearance: {
			refine: true,
			init: "atomfield"
		},

		value: {
			nullable: true,
			apply: "_applyValue",
			init: 0,
			event: "changeValue"
		},
	},

	members: {
		/**
		 * @inheritdoc
		 */
		getValue() {
			return this.getLabel();
		},

		/**
		 * @inheritdoc
		 */
		_applyValue(value) {
			if(value !== null) this.setLabel(value + "");
			else this.setLabel(null);
		},

		/**
		 * @inheritdoc
		 */
		resetValue() {
			this.setLabel(null);
			this.setIcon(null);
		}
	}
});