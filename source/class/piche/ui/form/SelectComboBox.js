/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved ComboBox widget.
 * This class add support to an ongoing selecetion by the input text
 */
qx.Class.define('piche.ui.form.SelectComboBox', {
	extend: qx.ui.form.ComboBox,

	construct() {
		super();

		this.getChildControl("textfield")
			.addListener("input", this.__handleTextInput, this);

		this.getChildControl("list")
			.removeListener("changeSelection", this._onListChangeSelection, this);
	},

	members: {
		// override
		_onTextFieldChangeValue(e) {
			let value = e.getData();

			if(value) {
				value = value.toLowerCase();

				// Search a first match
				let foundItem = this.__findFirst(e.getData());
				if(foundItem) this.setValue(foundItem.getLabel());
				else if(this.getChildControl("list").hasChildren()) {
					this.setValue(this.getChildControl("list").getChildren()[0].getLabel());
				}
			}
		},

		/**
		 * @param dataEvent {qx.event.type.Data}
		 * @private
		 */
		__handleTextInput(dataEvent) {
			// Ensure popup is open while typing
			if(!this.getChildControl("popup").isVisible()) this.open();

			// Search a match
			let foundItem = this.__findFirst(dataEvent.getData());
			if(foundItem) this.getChildControl("list").setSelection([foundItem]);
		},

		__getItemLabel: function(item) {
			if (item.isRich()) {
				let control = item.getChildControl("label", true);
				if (control) {
					let labelNode = control.getContentElement().getDomElement();
					if (labelNode) return qx.bom.element.Attribute.get(labelNode, "text");
					else return null;
				}
			}
			else return item.getLabel();
		},

		__findFirst(search) {
			// search must be case insensitive
			search = search.toLowerCase();

			let list = this.getChildControl("list");
			let items = list.getChildren();

			// go through all items
			for(let i in items) {
				let label = this.__getItemLabel(items[i]);

				if(label) {
					label = label.toLowerCase();
					if(label.includes(search)) return items[i];
				}
			}

			return null;
		}
	}
});