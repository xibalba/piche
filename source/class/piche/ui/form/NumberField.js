/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * The NumberField is a single-line text input field that support only number values
 */
qx.Class.define('piche.ui.form.NumberField', {
	extend: qx.ui.form.TextField,

	members: {
		/**
		 * Sets the value of the numberfield to the given value.
		 *
		 * @param value {Number} The new value
		 */
		setValue : function(value) {
			// handle null values
			if(value === null) return this.base(arguments, value);
			else if (qx.lang.Type.isNumber(value)) {
				this.base(arguments, String(value));
				return value;
			}

			throw new Error("Invalid value type: " + value);
		},

		/**
		 * Returns the current value of the numberfield.
		 * @return {Number} The current value
		 */
		getValue: function() {
			return Number(this.base(arguments));
		}
	}
});