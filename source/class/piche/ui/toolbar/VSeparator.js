/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2017 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * A widget used for decoration proposes to structure a toolbar. Each
 * Separator renders a line between the buttons around.
 */
qx.Class.define("piche.ui.toolbar.VSeparator", {
	extend: qx.ui.toolbar.Separator,

	properties: {
		// overridden
		appearance: {
			refine : true,
			init : "toolbar-v-separator"
		}
	}
});