/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class provide an Ideable toolbar Button.
 *
 * @see piche.mixins.WidgetIdeable
 */
qx.Class.define('piche.ui.toolbar.Button', {
    extend: qx.ui.toolbar.Button,
    include: [piche.mixins.WidgetIdeable],

    construct: function(caption, icon, command, widgetId) {
        this.base(arguments, caption, icon, command);
        if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.setWidgetId(widgetId);
    }
});
