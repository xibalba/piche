/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved MenuButton, provide Id registry support.
 */
qx.Class.define('piche.ui.toolbar.MenuButton', {
	extend: qx.ui.toolbar.MenuButton,
	include: [
		piche.mixins.WidgetIdeable,
		piche.mixins.WidgetIdRegistry
	],

	construct: function(label, icon, menu, widgetId) {
		this.base(arguments, label, icon, menu);
		if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.setWidgetId(widgetId);
	}
});
