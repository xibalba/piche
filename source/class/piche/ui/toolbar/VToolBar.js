/* *

 */
qx.Class.define('piche.ui.toolbar.VToolBar', {
    extend: qx.ui.toolbar.ToolBar,

    construct: function() {
        this.base(arguments);
        this._setLayout(new qx.ui.layout.VBox());
    }
});