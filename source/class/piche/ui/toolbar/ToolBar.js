/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved ToolBar, provide Id registry support
 * and allow to build a vertical ToolBar.
 */
qx.Class.define('piche.ui.toolbar.ToolBar', {
    extend: qx.ui.toolbar.ToolBar,
    include: [piche.mixins.WidgetIdRegistry],

    construct: function(vertical) {
        vertical = vertical || false;

        this.base(arguments);
        if(vertical) this._setLayout(new qx.ui.layout.VBox());
    },

    members: {
    	// Overriden
		addSeparator : function() {
			if(piche.lang.Object.isInstanceOf(this._getLayout(), 'qx.ui.layout.VBox')) this.add(new piche.ui.toolbar.VSeparator());
			else this.add(new qx.ui.toolbar.Separator());
		},

        _afterAddChild: function(child) {
            var widgetId = null;
            if(child.getWidgetId) widgetId = child.getWidgetId();
            if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.register(child, widgetId);
        },

        _afterRemoveChild: function(child) {
            var widgetId = null;
            if(child.getWidgetId) widgetId = child.getWidgetId();
            if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.unregister(child, widgetId);
        }
    }
});