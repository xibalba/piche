/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2016 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * A Basic Panel widget
 *
 * A Panel is a Container that support the adding of widgets and the setting of a custom layout,
 * also provide from a captionbar and a statusbar.
 */
qx.Class.define("piche.ui.panel.Basic", {
	extend: qx.ui.core.Widget,
	include: [
		qx.ui.core.MRemoteChildrenHandling,
		qx.ui.core.MRemoteLayoutHandling,
		piche.mixins.BusyBlocker
	],

	events: {
		/**
		 * Fired if the panel is closed.
		 */
		"close": "qx.event.type.Event"
	},

	properties: {
		// overridden
		appearance: {
			refine: true,
			init: "panel"
		},

		/** The text of the caption */
		caption: {
			check: "String",
			apply: "_applyCaption",
			nullable: true
		},

		/** The icon of the caption */
		icon: {
			check : "String",
			nullable : true,
			apply : "_applyIcon",
			themeable : true
		},

		showCloseButton: {
			check : "Boolean",
			init : false,
			apply : "_applyShowCloseButton"
		}
	},

	/**
	 * @param caption {String} The caption text
	 * @param icon {String} The URL of the captionbar icon
	 */
	construct: function (caption, icon) {
		this.base(arguments);

		// Configure internal layout
		this._setLayout(new qx.ui.layout.VBox());

		// Force the creation of the captionbar and pane
		this._createChildControl("captionbar");
		this._createChildControl("pane");

		// Apply constructor parameters
		if(caption) this.setCaption(caption);
		if(icon) this.setIcon(icon);

		this._createChildControl("close-button");
	},

	members: {
		/**
		 * The children container needed by the {@link qx.ui.core.MRemoteChildrenHandling}
		 *
		 * @return {qx.ui.container.Composite} pan sub widget.
		 */
		getChildrenContainer: function() {
			return this.getChildControl("pane");
		},

		// overridden
		_createChildControlImpl: function(id, hash) {
			var control;

			switch(id) {
				case "pane":
					control = new qx.ui.container.Composite();
					this._add(control, {flex: 1});

					break;
				case "captionbar":
					var layout = new qx.ui.layout.HBox();
					layout.setAlignY("middle");
					control = new qx.ui.container.Composite(layout);
					this._add(control);

					break;
				case "statusbar":
					control = new qx.ui.container.Composite(new qx.ui.layout.HBox());
					this._add(control);
					control.add(this.getChildControl("statusbar-text"));

					break;
				case "statusbar-text":
					control = new qx.ui.basic.Label();
					control.setValue(this.getStatus());

					break;
				case "icon":
					control = new qx.ui.basic.Image(this.getIcon());
					this.getChildControl("captionbar").addAt(control, 0);

					break;
				case "caption":
					control = new qx.ui.basic.Label(this.getCaption());
					control.setWidth(0);
					control.setAllowGrowX(true);
					this.getChildControl("captionbar").add(control, {flex: 1});

					break;
				case "close-button":
					control = new qx.ui.form.Button();
					control.setFocusable(false);
					control.addListener("execute", this._onCloseButtonTap, this);
					this.getChildControl("captionbar").addAfter(control, this.getChildControl("caption"));

					break;
			}

			return control || this.base(arguments, id);
		},

		// property apply
		_applyCaption: function(value, old, name) {
			this.getChildControl(name).setValue(value);
		},

		// property apply
		_applyIcon : function(value, old, name) {
			this.getChildControl(name).setSource(value);

			if(value) this._showChildControl(name);
			else this._excludeChildControl(name);
		},

		// property apply
		_applyShowCloseButton: function (value, old) {
			if(value) this._showChildControl("close-button");
			else this._excludeChildControl("close-button");
		},

		/**
		 * Fires a "close" event when the close button is tapped.
		 */
		_onCloseButtonTap : function(e) {
			if(!this.isVisible()) return;

			this.hide();
			this.fireEvent("close");
		}
	}
});
