/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class compose an Atom aside to a Label.
 */
qx.Class.define('piche.ui.basic.MoleculeLabel', {
	extend: qx.ui.core.Widget,

	construct: function(label, icon, value) {

	},

	properties: {

	},

	members: {

	}
});