/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2018 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved Simple Table Model class.
 *
 * This class just add «setValuesById()» method.
 * Use this class when want to set a batch of values by id to the model.
 */
qx.Class.define('piche.ui.table.model.Filtered', {
	extend: qx.ui.table.model.Filtered,

	members: {
		setValuesById: function(values, rowIndex) {
			for(var idx in values) {
				this.setValueById(idx, rowIndex, values[idx]);
			}
		}
	}
});
