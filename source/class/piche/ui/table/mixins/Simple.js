/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2020 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Simple
 * This class provide a simple way for build Table widget with Simple model
 * from a JSON Object as source for colmuns ids and names.
 * The JSON Object must have the form:
 *
 *     {
 *         id: 'Foo',
 *         bar: 'Baz'
 *     }
 */
qx.Mixin.define('piche.ui.table.mixins.Simple', {
	members: {
		/**
		 * Set the width of a column.
		 *
		 * @param colId {String|Integer} The column whose width is to be set.
		 * @param width {Integer|String}
		 *   The width of the specified column. The width may be specified as
		 *   integer number of pixels (e.g. 100), a string representing percentage
		 *   of the inner width of the Table (e.g. "25%"), or a string
		 *   representing a flex width (e.g. "1*").
		 */
		setColumnWidth: function(colId, width) {
			if(qx.lang.Type.isString(colId)) colId = this.getTableModel().getColumnIndexById(colId);

			if(qx.lang.Type.isString(width)) this.getTableColumnModel().getBehavior().setWidth(colId, width);
			else super(colId, width);
		},

		/**
		 * Sets whether a certain column is visible.
		 *
		 * @param colId {Integer} the model index of the column.
		 * @param visible {Boolean} whether the column should be visible.
		 */
		setColumnVisible: function(colId, visible) {
			if(qx.lang.Type.isString(colId)) colId = this.getTableModel().getColumnIndexById(colId);
			this.getTableColumnModel().setColumnVisible(colId, visible);
		},

		/**
		 * Sets the data renderer of a column.
		 *
		 * @param colId {String|Integer} The column whose renderer is to be set.
		 * @param renderer {qx.ui.table.ICellRenderer} the new data renderer
		 *   the column should get.
		 *
		 * @return {qx.ui.table.ICellRenderer|null} If an old renderer was set and
		 *   it was not the default renderer, the old renderer is returned for
		 *   pooling or disposing.
		 */
		setColumnRenderer: function(colId, renderer) {
			if(qx.lang.Type.isString(colId)) colId = this.getTableModel().getColumnIndexById(colId);
			return this.getTableColumnModel().setDataCellRenderer(colId, renderer);
		},

		setColumnEditable(colId, editable) {
			if(qx.lang.Type.isString(colId)) colId = this.getTableModel().getColumnIndexById(colId);
			this.getTableModel().setColumnEditable(colId, editable);
		},

		/**
		 * Establish a value into the provided coordinates.
		 * @param colId {String|Integer}
		 * @param rowIdx {Integer}
		 *
		 * @param value {*}
		 */
		setCellValue: function(colId, rowIdx, value) {
			if(qx.lang.Type.isString(colId)) colId = this.getTableModel().getColumnIndexById(colId);
			this.getTableModel().setValue(colId, rowIdx, value);
		},

		/**
		 * Return a cell value for the coordinates provided.
		 *
		 * @param colId {String|Integer}
		 * @param rowIdx {Integer}
		 *
		 * @return {*}
		 */
		getCellValue: function(colId, rowIdx) {
			if(qx.lang.Type.isString(colId)) colId = this.getTableModel().getColumnIndexById(colId);
			return this.getTableModel().getValue(colId, rowIdx);
		},

		/**
		 * Returns the data renderer of a column.
		 *
		 * @param colId {String|Integer} the model id or index of the column.
		 * @return {qx.ui.table.ICellRenderer} the data renderer of the column.
		 */
		getColumnRenderer: function(colId) {
			if(qx.lang.Type.isString(colId)) colId = this.getTableModel().getColumnIndexById(colId);
			return this.getTableColumnModel().getDataCellRenderer(colId);
		}
	}
});