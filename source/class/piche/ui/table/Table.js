/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved Table Widget.
 * This class is extended from qx.ui.table.Table for set by default
 * a «qx.ui.table.columnmodel.Resize» object as tableColumnModel.
 * Also change the behavior when a table is editable, allowing by
 * default go cell by cell at the focused row on «Enter» keypress instead of
 * columns. Anyway, is posible change that behavior for use the Qooxdoo
 * default.
 */
qx.Class.define('piche.ui.table.Table', {
	extend: qx.ui.table.Table,

	construct: function(tableModel, custom) {
		custom = custom || {};

		if(!qx.lang.Object.contains(custom, 'tableColumnModel')) {
			custom.tableColumnModel = function(obj) {
				return new qx.ui.table.columnmodel.Resize(obj);
			};
		}

		super(tableModel, custom);
	},

	statics: {
		ROW_EDIT_BEHAVIOR: 0x00,
		COL_EDIT_BEHAVIOR: 0x01
	},

	properties: {
		editBehavior: { check: 'Number', init: 0x00 }
	},

	members: {
		_onKeyPress: function(evt) {
			if(!this.getEnabled()) return;

			if(this.isEditing()) {
				if(this.getEditBehavior() === piche.ui.table.Table.COL_EDIT_BEHAVIOR) super(evt);
				else {
					let identifier = evt.getKeyIdentifier();
					let consumed = true;

					if(evt.getModifiers() === 0 && identifier === 'Enter') {
						this.stopEditing();
						let oldFocusedCol = this.__focusedCol;
						this.moveFocusedCell(1, 0);

						if(this.__focusedCol != oldFocusedCol) consumed = this.startEditing();
						else{
							let oldFocusedRow = this.__focusedRow;
							this.moveFocusedCell(0, 1);

							if(this.__focusedRow != oldFocusedRow){
								this.setFocusedCell(0, this.__focusedRow, true);
								consumed = this.startEditing();
							}
						}

						if(consumed) {
							evt.preventDefault();
							evt.stopPropagation();
						}
					}
					else super(evt);
				}
			}
			else super(evt);
		}
	}
});
