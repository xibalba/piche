/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2023 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Simple
 * This class provide a simple way for build Table widget with Simple model
 * from a JSON Object as source for colmuns ids and names.
 * The JSON Object must have the form:
 *
 *     {
 *         id: 'Foo',
 *         bar: 'Baz'
 *     }
 */
qx.Class.define('piche.ui.table.Simple', {
	extend: piche.ui.table.Table,

	include: [ piche.ui.table.mixins.Simple ],

	construct: function(columnsMap, custom) {
		let ids = [];
		let cols = [];

		for (let i in columnsMap) {
			ids.push(i);
			cols.push(columnsMap[i]);
		}

		let tableModel = new piche.ui.table.model.Simple();
		tableModel.setColumns(cols, ids);

		super(tableModel, custom);
	}
});