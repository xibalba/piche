/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2018 Laboratorios informáticos Xibalbá
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * View interface for composed elements, which are Controllers or Composite widgets.
 */
qx.Interface.define('piche.ui.interfaces.ViewsAware', {
	members: {
		/**
		 * Return a widget instance of actual composed view element.
		 *
		 * @return {qx.ui.core.Widget}
		 */
		getView: function() {}
	}
});
