/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class provide a Widget Ideable Page for use with
 * TabViews than can handle widgets by id.
 */
qx.Class.define('piche.ui.tabview.Page', {
    extend: qx.ui.tabview.Page,
    include: [piche.mixins.WidgetIdeable],

    construct: function(label, icon, widgetId) {
        this.base(arguments, label, icon);
        if(qx.lang.Type.isString(widgetId) && widgetId.length > 0) this.setWidgetId(widgetId);
    }
});