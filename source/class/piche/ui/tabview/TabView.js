/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2017 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class provide an extended TabView that support a blockable behavior
 * for improve the user interface experience.
 *
 * Also, this class provide a «setActiveTab()» method, in a similar way to
 * Ext.tab.Panel does.
 *
 *	 var tabView = new piche.ui.tabview.TabView();
 *	 var tabPage = new piche.ui.tabview.Page('label', 'icon', 'someId');
 *	 var anotherPage = new piche.ui.tabview.Page('label', 'icon', 'anotherId');
 *
 *	 tabView.add(tabPage);
 *	 tabView.add(anotherPage);
 *
 *	 // So, you can set active tab by instance.
 *	 tabView.setActiveTab(tabPage);
 *
 * @class piche.ui.tabview.TabView
 */
qx.Class.define('piche.ui.tabview.TabView', {
	extend: qx.ui.tabview.TabView,
	include: [ piche.mixins.BusyBlocker ],

	members: {
		/**
		 * Try to active the given tab.
		 * @param {qx.ui.tabview.Page} tab
		 */
		setActiveTab: function(tab) {
			var tabs = this.getSelectables();
			// Check if tab is an index number
			if(qx.lang.Type.isNumber(tab) && qx.lang.Number.isInRange(tab, 0, tabs.length - 1)) {
				this.setSelection([tabs[tab]]);
				return tabs[tab];
			}
			else if(qx.lang.Type.isString(tab)) {
				// if is a string, try get a childControl
				var t = this.getChildControl(tab, true);
				if(qx.lang.Type.isObject(t)) {
					this.setSelection([t]);
					return t;
				}
				else return false;
			}
			else if(piche.lang.Object.isInstanceOf(tab, 'qx.ui.tabview.Page')) {
				//Check if tab already exist on tabview.
				var resTab = new qx.type.Array()
					.append(tabs)
					.filter(function(t){
						return t === tab
					})[0];

				//If tab does not exist on tabview, then add it.
				if(!qx.lang.Type.isObject(resTab)) {
					resTab = tab;
					this.add(resTab);
				}

				this.setSelection([resTab]);
				return resTab;
			}
			return false;
		},

		/**
		 * Remove all children (pages).
		 */
		removeAll: function() {
			var tabs = this.getChildren();
			while(tabs.length > 0) {
				tabs[0].fireEvent("close");
			}
		}
	}
});
