/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * BusyIndicator is a popup widget for display a little feedback to users meanwhile
 * something is busy or loading.
 * This widget is just the popup and do not block other widgets.
 *
 * This widget is used primarily by MBusyBlocker mixin.
 *
 * @asset(piche/loading16.gif)
 */
qx.Class.define('piche.ui.popup.BusyIndicator', {
	extend: qx.ui.popup.Popup,

	construct: function(label) {
		label = label || qx.locale.Manager.tr('Please wait...');
		this.base(arguments, new qx.ui.layout.Basic());

		this.__atom = new qx.ui.basic.Atom(label, 'piche/loading16.gif');

		this.setAutoHide(false);
		this.setPadding(3, 6, 3, 6);
		this.add(this.__atom);
	},

	members: {
		__atom: null,

		/**
		 * Set the caption to use.
		 *
		 * @param caption {String} Set the caption to use.
		 */
		setCaption: function(caption) {
			this.__atom.setLabel(caption);
		}
	}
});
