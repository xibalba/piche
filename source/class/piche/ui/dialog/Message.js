/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 - 2016 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class provide a simple alert window.
 */
qx.Class.define('piche.ui.dialog.Message', {
	extend : piche.ui.dialog.Abstract,

	/**
	* @param caption {String?null} Title of dialog.
	* @param message {String} Message to show.
	* @param icon {String?null} Icon to use
	*/
	construct : function(caption, message, icon) {
		caption = caption || "Message";
		icon = icon || "alert";

		this.base(arguments, caption, message, icon);
		this._createChildControl("ok");
	},

	statics : {
		DEFAULT_ICONS : {
			"alert" : "icon/48/status/dialog-information.png",
			"warning" : "icon/48/status/dialog-warning.png",
			"error" : "icon/48/status/dialog-error.png",
			"success" : "icon/48/actions/dialog-apply.png"
		}
	},

	members : {
		// overridden
		_createChildControlImpl : function(id) {
			var control;

			if(id == "ok") {
				control = new qx.ui.form.Button(this.tr('OK'));
				control.addListener('execute', function(e) { this.close(); }, this);
				this.getChildControl("buttons-bar").add(control);
			}

			return control || this.base(arguments, id);
		},

		// property apply
		_applyCaptionBarChange : function(value, old, name) {
			if (name == "icon") {
				if (this.self(arguments).DEFAULT_ICONS[value]) value = this.self(arguments).DEFAULT_ICONS[value];
				this.getChildControl("atom").setIcon(value);
			}
			else this.base(arguments, value, old, name);
		}
	}
});
