/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 - 2016 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(qx/icon/Oxygen/48/status/dialog-information.png)
 * @asset(qx/icon/Oxygen/48/status/dialog-warning.png)
 * @asset(qx/icon/Oxygen/48/status/dialog-error.png)
 * @asset(qx/icon/Oxygen/48/actions/dialog-apply.png)
 *
 * @asset(qx/icon/Oxygen/16/actions/dialog-apply.png)
 * @asset(qx/icon/Oxygen/16/actions/process-stop.png)
 */
qx.Theme.define("piche.theme.Appearance", {
	extend : qx.theme.indigo.Appearance,

	appearances : {
		"atomfield": {
			style: function(states) {
				return {
					decorator: "pseudoinset"
				}
			}
		},

		"dialog" : {
			include: "window",

			style: function(states) {
				return {
					contentPadding : [ 0, 0, 10, 0 ],
					decorator : states.maximized ? undefined : states.active ? "dialog-box" : "window",
					padding: 10
				}
			}
		},

		"dialog/atom" : {
			include: "atom"
		},

		"dialog/title" : {
			style : function(states) {
				return {
					font : "bold",
					//alignY : "middle",
					textAlign : "center"
				}
			}
		},

		"dialog/ok" : {
			alias : "button",
			include : "button",

			style : function(states) {
				return {
					icon : "icon/16/actions/dialog-ok.png"
				};
			}
		},

		"dialog/yes" : {
			alias : "button",
			include : "button",

			style : function(states) {
				return {
					icon : "icon/16/actions/dialog-apply.png"
				};
			}
		},

		"dialog/no" : {
			alias : "button",
			include : "button",

			style : function(states) {
				return {
					icon : "icon/16/actions/process-stop.png"
				};
			}
		},

		"dialog/cancel" : {
			alias : "button",
			include : "button",

			style : function(states) {
				return {
					icon : "icon/16/actions/dialog-cancel.png"
				};
			}
		},

		"panel": "widget",

		"panel/captionbar": {
			style: function (states) {
				return {
					padding : [3, 8, 1, 8],
					decorator: "window-caption-active"
				}
			}
		},

		"panel/caption": {
			style: function (states) {
				return {
					font: "bold"
				}
			}
		},

		"panel/icon": {
			style : function(states) {
				return {
					marginRight : 4
				};
			}
		},

		"panel/close-button": {
			alias : "atom",
			style : function(states) {
				return {
					cursor : "pointer",
					icon : qx.theme.simple.Image.URLS["tabview-close"]
				};
			}
		},

		"toolbar-v-separator" : {
			style : function(states) {
				return {
					height : 0,
					decorator : "toolbar-v-separator",
					marginTop : 4,
					marginBottom: 4,
					marginLeft : 7,
					marginRight : 7
				}
			}
		}
	}
});
