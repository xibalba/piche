/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 - 2016 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Theme.define("piche.theme.Theme", {
	meta : {
		color : piche.theme.Color,
		decoration : piche.theme.Decoration,
		font : piche.theme.Font,
		icon : qx.theme.icon.Oxygen,
		appearance : piche.theme.Appearance
	}
});
