/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 - 2016 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Theme.define("piche.theme.Decoration", {
	extend: qx.theme.indigo.Decoration,

	decorations : {
		pseudoinset: {
			style: {
				width: 1,
				color: [
					"white",
					"white",
					"border-light-shadow",
					"white"
				]
			}
		},

		"dialog-box": {
			include: "popup",
			style: {
				width: [4, 1, 1, 1],
				radius: [4, 4, 4, 4],
				backgroundColor : "background",
				color: ['highlight', 'window-border', 'window-border', 'window-border']
			}
		},

		"toolbar-v-separator": {
			style: {
				widthTop: 1,
				colorTop : "button-border"
			}
		}
	}
});
