/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class provide static methods for inflects strings.
 */
qx.Class.define('piche.lang.Inflector', {
    type: 'static',

    statics: {
        classify: function(str) {
            var result = qx.lang.String.capitalize(str);
            return result.replace(/[^A-Za-z0-9]/g, '');
        }
    }
});
