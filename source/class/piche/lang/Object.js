/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This class provide extra static methods for Object manipulation.
 * Since the methods are static, this class is not a qx.lang.Object extension.
 */
qx.Class.define('piche.lang.Object', {
    type: 'static',

    statics: {
        isInstanceOf: function(object, classname){
            if (!object) return false;

            var clazz = object.constructor;
            while(clazz) {
                if (clazz.classname === classname) return true;
                clazz = clazz.superclass;
            }
            return false;
        }
    }
});
