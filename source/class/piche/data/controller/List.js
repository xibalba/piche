/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2018 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * ## List Controller
 * This class extend the qooxdoo Data List Controller adding some API methods for retrive models data
 */
qx.Class.define('piche.data.controller.List', {
	extend: qx.data.controller.List,

	members: {
		getModelByField: function(field, value) {
			var models = this.getModel();
			var lenght = models.getLength();

			for(var i = 0; i < lenght; i++) {
				var m = models.getItem(i);
				if(m.get(field) === value) return m;
			}

			return null;
		},

		setSelectionByField: function(field, value) {
			var model = this.getModelByField(field, value);

			if(model) {
				var selection = this.getSelection();

				selection.removeAll();
				selection.push(model.get(field));

				this.setSelection(selection);
			}
		}
	}
});
