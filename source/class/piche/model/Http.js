/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2019 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * HTTP model with related constants and static variables.
 */
qx.Class.define('piche.model.Http', {
	extend: qx.core.Object,

	statics: {
		DATE_FORMAT: "D, d M Y H:i:s T",
		HTTP_10: "HTTP/1.0",
		HTTP_11: "HTTP/1.1",

		CONTINUE_REQUEST: 100,
		SWITCHING_PROTOCOLS: 101,
		PROCESSING: 102,
		OK: 200,
		CREATED: 201,
		ACCEPTED: 202,
		NON_AUTHORITATIVE_INFORMATION: 203,
		NO_CONTENT: 204,
		RESET_CONTENT: 205,
		PARTIAL_CONTENT: 206,
		MULTI_STATUS: 207,
		MULTIPLE_CHOICES: 300,
		MOVED_PERMANENTLY: 301,
		FOUND: 302,
		SEE_OTHER: 303,
		NOT_MODIFIED: 304,
		USE_PROXY: 305,
		TEMPORARY_REDIRECT: 307,
		BAD_REQUEST: 400,
		UNAUTHORIZED: 401,
		PAYMENT_REQUIRED: 402,
		FORBIDDEN: 403,
		NOT_FOUND: 404,
		METHOD_NOT_ALLOWED: 405,
		NOT_ACCEPTABLE: 406,
		PROXY_AUTHENTICATION_REQUIRED: 407,
		REQUEST_TIMEOUT: 408,
		CONFLICT: 409,
		GONE: 410,
		LENGTH_REQUIRED: 411,
		PRECONDITION_FAILED: 412,
		REQUEST_ENTITY_TOO_LARGE: 413,
		REQUEST_URI_TOO_LONG: 414,
		UNSUPPORTED_MEDIA_TYPE: 415,
		REQUESTED_RANGE_NOT_SATISFIABLE: 416,
		EXPECTATION_FAILED: 417,
		UNPROCESSABLE_ENTITY: 422,
		LOCKED: 423,
		FAILED_DEPENDENCY: 424,
		PRECONDITION_REQUIRED: 428,
		TOO_MANY_REQUESTS: 429,
		REQUEST_HEADER_FIELDS_TOO_LARGE: 431,
		INTERNAL_SERVER_ERROR: 500,
		NOT_IMPLEMENTED: 501,
		BAD_GATEWAY: 502,
		SERVICE_UNAVAILABLE: 503,
		GATEWAY_TIMEOUT: 504,
		HTTP_VERSION_NOT_SUPPORTED: 505,
		INSUFFICIENT_STORAGE: 507,
		LOOP_DETECTED: 508,
		NOT_EXTENDED: 510,
		NETWORK_AUTHENTICATION_REQUIRED: 511,

		/**
		 * List of acceptable header types.
		 */
		_headerTypes: [
			'Accept',
			'Accept-Charset',
			'Accept-Encoding',
			'Accept-Language',
			'Accept-Ranges',
			'Access-Control-Allow-Origin',
			'Age',
			'Allow',
			'Authentication-Info',
			'Authorization',
			'Cache-Control',
			'Connection',
			'Content-Disposition',
			'Content-Encoding',
			'Content-Language',
			'Content-Length',
			'Content-Location',
			'Content-MD5',
			'Content-Range',
			'Content-Type',
			'Cookie',
			'Date',
			'ETag',
			'Expires',
			'Expect',
			'From',
			'Host',
			'If-Match',
			'If-Modified-Since',
			'If-None-Match',
			'If-Range',
			'If-Unmodified-Since',
			'Keep-Alive',
			'Last-Modified',
			'Link',
			'Location',
			'Max-Forwards',
			'Origin',
			'P3P',
			'Pragma',
			'Proxy-Authenticate',
			'Proxy-Authorization',
			'Range',
			'Referer',
			'Refresh',
			'Retry-After',
			'Server',
			'Set-Cookie',
			'Status',
			'Strict-Transport-Security',
			'TE',
			'Trailer',
			'Transfer-Encoding',
			'Upgrade',
			'User-Agent',
			'Vary',
			'Via',
			'Warning',
			'WWW-Authenticate'
		],

		/**
		 * List of possible method types.
		 */
		_methodTypes: [
			'GET',
			'POST',
			'PUT',
			'DELETE',
			'HEAD',
			'TRACE',
			'OPTIONS',
			'CONNECT'
		],

		/**
		 * List of all available response status codes.
		 */
		_statusCodes: {
			100: 'Continue',
			101: 'Switching Protocols',
			102: 'Processing', // RFC2518
			200: 'OK',
			201: 'Created',
			202: 'Accepted',
			203: 'Non-Authoritative Information',
			204: 'No Content',
			205: 'Reset Content',
			206: 'Partial Content',
			207: 'Multi-Status', // RFC4918
			300: 'Multiple Choices',
			301: 'Moved Permanently',
			302: 'Found',
			303: 'See Other',
			304: 'Not Modified',
			305: 'Use Proxy',
			307: 'Temporary Redirect',
			400: 'Bad Request',
			401: 'Unauthorized',
			402: 'Payment Required',
			403: 'Forbidden',
			404: 'Not Found',
			405: 'Method Not Allowed',
			406: 'Not Acceptable',
			407: 'Proxy Authentication Required',
			408: 'Request Timeout',
			409: 'Conflict',
			410: 'Gone',
			411: 'Length Required',
			412: 'Precondition Failed',
			413: 'Request Entity Too Large',
			414: 'Request-URI Too Long',
			415: 'Unsupported Media Type',
			416: 'Requested Range Not Satisfiable',
			417: 'Expectation Failed',
			422: 'Unprocessable Entity', // RFC4918
			423: 'Locked', // RFC4918
			424: 'Failed Dependency', // RFC4918
			428: 'Precondition Required', // RFC6585
			429: 'Too Many Requests', // RFC6585
			431: 'Request Header Fields Too Large', // RFC6585
			500: 'Internal Server Error',
			501: 'Not Implemented',
			502: 'Bad Gateway',
			503: 'Service Unavailable',
			504: 'Gateway Timeout',
			505: 'HTTP Version Not Supported',
			507: 'Insufficient Storage', // RFC4918
			508: 'Loop Detected', // RFC5842
			510: 'Not Extended', // RFC2774
			511: 'Network Authentication Required' // RFC6585
		},

		/**
		 * Return all the standard types of HTTP headers.
		 *
		 * @return Array
		 */
		getHeaderTypes: function() {
			return this.self(arguments)._headerTypes;
		},

		/**
		 * Return all the supported method types.
		 *
		 * @return Array
		 */
		getMethodTypes: function() {
			return this.self(arguments)._methodTypes;
		},

		/**
		 * Get a single status code.
		 *
		 * @return String
		 */
		getStatusCode: function(code) {
			if(this.self(arguments)._statusCodes[code]) return this.self(arguments)._statusCodes[code];
			throw new Error("Status code " + code + " is not supported");
		},

		/**
		 * Get all status codes.
		 *
		 * @return Array
		 */
		getStatusCodes: function() {
			return this.self(arguments)._statusCodes;
		}
	}
});
