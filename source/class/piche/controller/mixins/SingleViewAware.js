/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015-2016 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Ths mixin provide an easy way for handling a single views by controllers.
 */
qx.Mixin.define('piche.controller.mixins.SingleViewAware', {
	properties: {
		mainView: {
			check: "qx.ui.core.Widget"
		}
	}
});