/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Mixin.define('piche.controller.mixins.ProfileAware', {
	members: {
		__userProfile: null,

		_setProfile: function(data) {
			if(!this.__userProfile) this.__userProfile = {};
			this.__userProfile = data;
		},

		getProfile: function(key) {
			key = key || false;

			if(!key) return this.__userProfile;
			if(this.__userProfile[key] !== undefined) return this.__userProfile[key];

			throw new Error(key + '-> Unsupported key for user data.');
		}
	}
});
