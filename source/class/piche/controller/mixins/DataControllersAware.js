/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2017 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Mixin DataControllersAware
 * This mixin provide a basic implementation for DataFormControllable interface.
 */
qx.Mixin.define('piche.controller.mixins.DataControllersAware', {
	members: {
		/**
		 * @inheritdoc
		 */
		_getFormModel: function(id) {
			var controller = this._getFormController(id);
			controller.updateModel();
			return controller.getModel();
		},

		/**
		 * @inheritdoc
		 */
		_getFormData: function(id) {
			return qx.util.Serializer.toNativeObject(this._getFormModel(id));
		},

		/**
		 * @inheritdoc
		 */
		_getSerializedFormData: function(id, asJson) {
			asJson = asJson || true;
			if(asJson) return qx.util.Serializer.toJson(this._getFormModel(id));
			else return qx.util.Serializer.toUriParameter(this._getFormModel(id));
		},

		/**
		 * @inheritdoc
		 */
		_resetFormData: function(id) {
			this._getFormController(id).resetModet();
			this._getFormController(id).createModel();
		}
	}
});