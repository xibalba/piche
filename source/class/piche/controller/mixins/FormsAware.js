/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This mixin provide to controllers methods for data form handling
 *
 * Use this mixins for provide to yours controllers a structured way of
 * form data manipulation.
 */
qx.Mixin.define('piche.controller.mixins.FormsAware', {
	members: {
		/**
		 * Devuelve el mapa interno de Controladores.
		 * @returns {Object}
		 * @protected
		 */
		_getFormControllersMap: function() {
			var map = this.getUserData('innerFormControllersMap');
			if(map) return map;
			else {
				map = {};
				this.setUserData('innerFormControllersMap', map);
				return map;
			}
		},

		/**
		 * This method create an qx.data.controller.Form, bind tho passed form and register with an id.
		 *
		 * @param id {String}
		 * @param form {qx.ui.form.Form}
		 * @protected
		 */
		_registerFormDataController: function(id, form) {
			var map = this._getFormControllersMap();
			if(map[id]) throw new Error("A form data controller with the id '"+id+"' already exists on" + this.classname + ".");

			var formController = new qx.data.controller.Form(null, form, true);
			formController.createModel();
			map[id] = formController;
		},

		/**
		 * This method return the registred form data controller for the pased id.
		 * If an invalid is pased, an Error is rised.
		 *
		 * @param id {String}
		 * @returns {qx.data.controller.Form}
		 * @protected
		 */
		_getFormDataController: function(id) {
			var map = this._getFormControllersMap();
			if(map[id]) return map[id];
			else throw new Error("Form data controller with the id '"+id+"' is not registeren on" + this.classname + ".");
		},

		/**
		 * Returns a map of actual controller model status.
		 *
		 * @param id {String}
		 * @returns {Object}
		 * @protected
		 */
		_getFormData: function(id) {
			var formController = this._getFormDataController(id);
			formController.updateModel();
			return qx.util.Serializer.toNativeObject(formController.getModel());
		},

		/**
		 * Returns a serialized form data for use on request.
		 *
		 * @param id {String}
		 * @param asJson {Boolean}
		 * @returns {Object|String}
		 * @protected
		 */
		_getSerializedFormData: function(id, asJson) {
			asJson = asJson || true;
			var formController = this._getFormDataController(id);

			formController.updateModel();
			if(asJson) return qx.util.Serializer.toJson(formController.getModel());
			else return qx.util.Serializer.toUriParameter(formController.getModel());
		},

		/**
		 * Set data to binded form.
		 *
		 * @param id {String}
		 * @param data {Map}
		 * @protected
		 */
		_setFormData: function(id, data) {
			var formController = this._getFormDataController(id);
			formController.getModel().set(data);
		},

		/**
		 * Remove form data controllers from registry.
		 * @protected
		 */
		_unregisterFormDataControllers: function() {
			var map = this._getFormControllersMap();
			map = {};
		},

		/**
		 * Reset the current model for the pased id and create a new one ready for set new data
		 *
		 * @param id {String}
		 * @protected
		 */
		_resetFormData: function(id) {
			var formController = this._getFormDataController(id);
			formController.resetModel();
			formController.createModel();
		}
	}
});
