/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This mixin provide an short way for assign Command to widgets.
 */
qx.Mixin.define('piche.controller.mixins.CommandRegistrable', {
	members: {
		/**
		 * Register a function (fn param) to the passed view as
		 * a command to be raised by the «execute» listener. Also you can
		 * assign a shortcut for the command and a scope for the function.
		 *
		 * @param view {qx.ui.core.Widget} The view to assign the function.
		 * @param fn {Function} The function to be executed.
		 * @param shortcut {String} Optional.
		 * @param scope {Object} Optional, this by default.
		 */
		registerCommand: function(view, fn, shortcut, scope) {
			scope = scope || this;

			view.setCommand(new qx.ui.command.Command(shortcut));
			view.getCommand().addListener("execute", fn, scope);
		}
	}
});
