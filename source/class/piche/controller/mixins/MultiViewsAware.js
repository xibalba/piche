/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Ths mixin provide an easy way for handling views by controllers.
 */
qx.Mixin.define('piche.controller.mixins.MultiViewsAware', {
	members: {
		/**
		 * Returns the inner viewsMap.
		 *
		 * @protected
		 */
		_getViewsMap: function() {
			var viewsMap = this.getUserData('innerViewsMap');
			if(viewsMap) return viewsMap;
			else {
				viewsMap = [];
				this.setUserData('innerViewsMap', viewsMap);
				return viewsMap;
			}
		},

		/**
		 * Add a view to the controller identified by an id.
		 *
		 * @param view {qx.ui.core.Widget} Widget view to add.
		 * @param id {String} Id than must be assigned to the view.
		 */
		addView: function(view, id) {
			var viewsMap = this._getViewsMap();

			if(viewsMap[id]) throw new Error("A view with the id '"+id+"' already exists on" + this.classname + ".");
			viewsMap[id] = view;

			if (qx.core.Environment.get("qx.debug")) {
				console.info('Added view to '+ this.classname + ' controller with id: ' + id);
			}
		},

		/**
		 * Retrieve a view by its id from the controller.
		 *
		 * @param id {String} The id of the view to retrieve.
		 * @returns {qx.ui.core.Widget} The correspanding view.
		 */
		getView: function(id) {
			var viewsMap = this._getViewsMap();
			if(viewsMap[id]) return viewsMap[id];
			else throw new Error("A view with the id '"+id+"' does not exists on" + this.classname + " controller.");
		},

		/**
		 * Remove an id from the controller.
		 *
		 * @param id {String} The id of the view to remove.
		 */
		removeView: function(id) {
			var view = this.getView(id);
			if(view) view.destroy();
			delete(this._getViewsMap()[id]);

			if (qx.core.Environment.get("qx.debug")) {
				console.info('Removed view from '+ this.classname + ' controller with id: ' + id);
			}
		},

		removeViews: function() {
			var viewsMap = this._getViewsMap();
			for(var id in viewsMap) this.removeView(id);
		}
	}
});