/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2017 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Base class for controllers over MVC architecture.
 *
 * This abstract class is the composition of the ViewsAware and Dispatchable interfaces,
 * so each controller must implement a factory method to retrieve the view to handle,
 * also must implement the `dispatch()` method as start point for the controller.
 *
 * So this class do not provide any functionality, is just a "templeate" for build controllers.
 */
qx.Class.define('piche.controller.BaseAbstract', {
	extend: qx.core.Object,
	type: 'abstract',

	implement: [
		piche.ui.interfaces.ViewsAware,
		piche.controller.interfaces.Dispatchable
	]
});
