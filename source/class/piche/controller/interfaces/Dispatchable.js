/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2017 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Interface.define('piche.controller.interfaces.Dispatchable', {
	members: {
		/**
		 * This method must be implemented as a logic launcher for controllers.
		 */
		dispatch: function() {}
	}
});
