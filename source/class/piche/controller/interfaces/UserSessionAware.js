/* ****************************************************************

 @link https://bitbucket.org/xibalba/piche
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Controller interface for those cases when is required handle user session.
 */
qx.Interface.define('piche.controller.interfaces.UserSessionAware', {
	members: {
		/**
		 * Check for existing session, for example if exist an `PHPSESSID` cookie or
		 * any other session method.
		 *
		 * @return {Boolean}
		 */
		_checkSession: function() {},

		/**
		 * Handle login process.
		 */
		_handleLogin: function() {},

		/**
		 * Handle logout process.
		 */
		_handleLogout: function() {}
	}
});
