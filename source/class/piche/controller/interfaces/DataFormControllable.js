/* ****************************************************************

 @link https://gitlab.com/xibalba/piche
 @copyright 2015 - 2017 Xibalba Lab
 @license MIT: https://opensource.org/licenses/MIT

 -- Piche Library --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Interface DataFormControllable
 * This interface define an API for manipulate Form widgets with qooxdoo data form controllers,
 * those controllers ever are `qx.data.controller.Form` instances. The major part of this interface
 * is implemented by DataControllersAware mixin, but the `_getFormController()` method must be
 * defined into the target class.
 */
qx.Interface.define('piche.controller.interfaces.DataFormControllable', {
	members: {
		/**
		 * Return a form controller instance.
		 *
		 * @param id {String} An unique identifier for retrive the form controller instance.
		 * @returns {qx.data.controller.Form}
		 */
		_getFormController: function(id) {},

		/**
		 * Return an updated form model object from the matched id of an supported form controller.
		 *
		 * @param id {String} An unique identifier for retrive the form model instance.
		 * @returns {qx.core.Object}
		 */
		_getFormModel: function(id) {},

		/**
		 * Return an object map (a JS native object) cerated from the form model for the
		 * matched id form controller.
		 * 
		 * @param id {String} An unique identifier for retrive the form data.
		 * @returns {Object}
		 */
		_getFormData: function(id) {},

		/**
		 * Return a serialized formated form data.
		 * This formated data is useful for JSON manipulation or as part of request data.
		 * 
		 * @param id {String} An unique identifier for retrive the form serialized data.
		 * @param asJson {Booleat}
		 * 
		 * @returns {Object | String}
		 */
		_getSerializedFormData: function(id, asJson) {},

		/**
		 * Reset the form model for the matched id of a supported form data controller.
		 * @param id {String} An unique identifier for reset the form data controller.
		 */
		_resetFormData: function(id) {}
	}
});